function check() {
    var x = document.getElementById("t1");
    var y = document.getElementById("t2");
    var z = document.getElementById("t3");
    var a = +x.value;
    var b = +y.value;
    var c = +z.value;
    if (a == b && b == c) {
        document.getElementById("l1").innerHTML = "the triangle is equilateral";
    }
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && c != b)) {
        document.getElementById("l2").innerHTML = "triangle is isocles";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2) ||
            Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2))) {
            document.getElementById("l2").innerHTML = "triangle is also a right angled triangle";
        }
    }
    else if (a != b && b != c && a !== c) {
        document.getElementById("l1").innerHTML = "triangle is scalene";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2) ||
            Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2))) {
            document.getElementById("l2").innerHTML = "triangle is also a right angled triangle";
        }
    }
}
//# sourceMappingURL=app.js.map