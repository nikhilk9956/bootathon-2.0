function promfn() {
    let a = document.getElementById("t1");
    let i;
    let num;
    var positive_count = 0;
    var negative_count = 0;
    var zero_count = 0;
    var count = +a.value;
    for (i = 0; i < count; i++) {
        num = +prompt("Enter a number");
        if (num > 0) {
            positive_count++;
        }
        else if (num < 0) {
            negative_count++;
        }
        else {
            zero_count++;
        }
    }
    document.getElementById("show").innerHTML += ("<b>Number of positive numbers:  </b>") + positive_count;
    document.getElementById("show").innerHTML += ("<br><b>Number of negative numbers:  </b>") + negative_count;
    document.getElementById("show").innerHTML += ("<br><b>Number of zero numbers:  </b>") + zero_count;
}
//# sourceMappingURL=Prompt.js.map